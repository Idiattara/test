import org.apache.spark.sql.SparkSession

object Tes{
  def main(args: Array[String]): Unit = {
    implicit val spark: SparkSession = SparkSession
      .builder
      .appName("MSD")
      .config("spark.master", "local")
      .getOrCreate()
    val path="/home/diattara/IdeaProjects/test/src/test.csv"
      val df=spark.sqlContext.read.format("csv")
        .option("delimiter", ",")
        .option("header", true)
        .load(path)

df.show()

  }
}